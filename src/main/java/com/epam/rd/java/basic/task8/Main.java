package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import java.io.FileInputStream;
import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		var flowers = domController.getFlowers();
		// sort (case 1)
		// PLACE YOUR CODE HERE

		flowers.sort(Comparator.comparing(Flower::getName));
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.safeFile(outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp = spf.newSAXParser();
		sp.parse("input.xml", saxController );
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		var list = saxController.getFlowers();
		list.sort((Comparator.comparingInt(Flower::getCm)));
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.safeFile(outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		var list1 = staxController.getContent();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		list1.sort(Comparator.comparing(Flower::getStemColour));
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.safeFile(outputXmlFile);
	}

}
