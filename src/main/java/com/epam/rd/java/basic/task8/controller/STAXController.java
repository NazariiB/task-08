package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private final List<Flower> flowers = new ArrayList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	 public List<Flower> getContent() throws Exception {
		Flower flower = null;
		 XMLInputFactory xif = XMLInputFactory.newInstance();
		 XMLEventReader xer = xif.createXMLEventReader(new FileInputStream(xmlFileName));
		 while (xer.hasNext()){
			 XMLEvent event = xer.nextEvent();
			 if(event.isStartElement()){
				 StartElement se = event.asStartElement();
				 if(se.getName().getLocalPart().equals("flower")){
					 flower = new Flower();
				 }
				 if(se.getName().getLocalPart().equals("name")){
					 flower.setName(xer.nextEvent().asCharacters().getData());
				 }
				 if(se.getName().getLocalPart().equals("soil")){
					 flower.setSoil(xer.nextEvent().asCharacters().getData());
				 }
				 if(se.getName().getLocalPart().equals("origin")){
					 flower.setOrigin(xer.nextEvent().asCharacters().getData());
				 }
				 if(se.getName().getLocalPart().equals("stemColour")){
					 flower.setStemColour(xer.nextEvent().asCharacters().getData());
				 }
				 if(se.getName().getLocalPart().equals("leafColour")){
					 flower.setLeafColour(xer.nextEvent().asCharacters().getData());
				 }
				 if(se.getName().getLocalPart().equals("aveLenFlower")){
					 flower.setCm(Integer.parseInt(xer.nextEvent().asCharacters().getData()));
				 }
				 if(se.getName().getLocalPart().equals("tempreture")){
					 flower.setCelcius(Integer.parseInt(xer.nextEvent().asCharacters().getData()));
				 }
				 if(se.getName().getLocalPart().equals("lighting")){
					 Attribute atr = se.getAttributeByName(new QName("lightRequiring"));
					 flower.setLightRequiring(atr.getValue().equals("yes"));
				 }
				 if(se.getName().getLocalPart().equals("watering")){
					 flower.setMlPerWeek(Integer.parseInt(xer.nextEvent().asCharacters().getData()));
				 }
				 if(se.getName().getLocalPart().equals("multiplying")){
					 flower.setMultiplying(xer.nextEvent().asCharacters().getData());
				 }
			 }
			 if(event.isEndElement()){
				 EndElement ee = event.asEndElement();
				 if(ee.getName().getLocalPart().equals("flower")){
					 flowers.add(flower);
				 }
			 }
		 }
		return flowers;
	 }

	public void safeFile(String xmlFileName) throws Exception {
		var file = new File(xmlFileName);
		PrintWriter writer = new PrintWriter(file);
//		if(file.length() != 0)
		writer.print("<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n");
		for (var flower:flowers) {
			writer.print(convertToWML(flower));
		}
		writer.print("</flowers>");
		writer.close();
	}

	private String convertToWML(Flower flower){
		return "<flower>\n<name>"+ flower.getName() + "</name>\n<soil>" +
				flower.getSoil() + "</soil>\n<origin>" +
				flower.getOrigin() + "</origin>\n<visualParameters>\n<stemColour>" +
				flower.getStemColour() + "</stemColour>\n<leafColour>" +
				flower.getLeafColour() + "</leafColour>\n<aveLenFlower measure=\"cm\">" +
				flower.getCm() + "</aveLenFlower>\n</visualParameters>\n<growingTips>\n<tempreture measure=\"celcius\">" +
				flower.getCelcius() + "</tempreture>\n<lighting lightRequiring=" +
				(flower.isLightRequiring()?"\"no\"":"\"yes\"") + "/>\n<watering measure=\"mlPerWeek\">" +
				flower.getMlPerWeek() + "</watering>\n</growingTips>\n<multiplying>" +
				flower.getMultiplying() + "</multiplying>\n</flower>";
	}

}