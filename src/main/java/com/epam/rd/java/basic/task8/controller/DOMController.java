package com.epam.rd.java.basic.task8.controller;


import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private List<Flower> flowers = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	private String getTagValue(String tag, Element element){
		NodeList nl = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = nl.item(0);
		return node.getNodeValue();
	}

	public List<Flower> getFlowers() throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		var document =  db.parse(new File("input.xml"));
		NodeList nl = document.getDocumentElement().getElementsByTagName("flower");
		for (int i = 0; i < nl.getLength(); i++) {
			Node flower = nl.item(i);
			if(flower.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element) flower;
				var name = getTagValue("name", element);
				var soil = getTagValue("soil", element);
				var origin = getTagValue("origin", element);
				var steamColour = getTagValue("stemColour", element);
				var leafColour = getTagValue("leafColour", element);
				var aveLenFlower = getTagValue("aveLenFlower", element);
				var tempreture = getTagValue("tempreture", element);
				var lighting = element.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent();
				var watering = getTagValue("watering", element);
				var multiplying = getTagValue("multiplying", element);
				flowers.add(new Flower(name, soil, origin, steamColour, leafColour, Integer.parseInt(aveLenFlower),
						Integer.parseInt(tempreture), lighting == "yes", Integer.parseInt(watering), multiplying));
			}
			System.out.println("-----");
		}
		return flowers;
	}

	public void safeFile(String xmlFileName) throws Exception {
		var file = new File(xmlFileName);
		PrintWriter writer = new PrintWriter(file);
//		if(file.length() != 0)
			writer.print("<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n");
		for (var flower:flowers) {
			writer.print(convertToWML(flower));
		}
		writer.print("</flowers>");
		writer.close();
	}

	private String convertToWML(Flower flower){
		return "<flower>\n<name>"+ flower.getName() + "</name>\n<soil>" +
				flower.getSoil() + "</soil>\n<origin>" +
				flower.getOrigin() + "</origin>\n<visualParameters>\n<stemColour>" +
				flower.getStemColour() + "</stemColour>\n<leafColour>" +
				flower.getLeafColour() + "</leafColour>\n<aveLenFlower measure=\"cm\">" +
				flower.getCm() + "</aveLenFlower>\n</visualParameters>\n<growingTips>\n<tempreture measure=\"celcius\">" +
				flower.getCelcius() + "</tempreture>\n<lighting lightRequiring=" +
				(flower.isLightRequiring()?"\"no\"":"\"yes\"") + "/>\n<watering measure=\"mlPerWeek\">" +
				flower.getMlPerWeek() + "</watering>\n</growingTips>\n<multiplying>" +
				flower.getMultiplying() + "</multiplying>\n</flower>";
	}

}
