package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	private  String name, soil, origin, stemColour, leafColour, multiplying;
	private  int cm, celcius, mlPerWeek;
	private  boolean lightRequiring;

	private String xmlFileName;
	private String name1;
	private List<Flower> flowers = new ArrayList<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String info = String.valueOf(ch).substring(start, start + length).trim();
		if(info.isBlank())
			return;
		if(name1 != null) {
			if (name1.equals("name"))
				name = info;
			if (name1.equals("soil"))
				soil = info;
			if (name1.equals("origin"))
				origin = info;
			if (name1.equals("stemColour"))
				stemColour = info;
			if (name1.equals("leafColour"))
				leafColour = info;
			if (name1.equals("aveLenFlower"))
				cm = Integer.parseInt(info);
			if (Objects.equals(name1, "tempreture"))
				celcius = Integer.parseInt(info);
			if (Objects.equals(name1, "watering"))
				mlPerWeek = Integer.parseInt(info);
			if (Objects.equals(name1, "multiplying"))
				multiplying = info;
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if(qName.equals("lighting")){
			lightRequiring = attributes.getValue("lightRequiring") == "yes";
		}else {
			name1 = qName;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName == "flower")
			flowers.add(new Flower(name, soil, origin, stemColour,
					leafColour, cm, celcius, lightRequiring, mlPerWeek,
					multiplying));
		name1 = null;
	}

	public List<Flower> getFlowers() {
		return flowers;
	}

	public void safeFile(String xmlFileName) throws Exception {
		var file = new File(xmlFileName);
		PrintWriter writer = new PrintWriter(file);
//		if(file.length() != 0)
		writer.print("<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n");
		for (var flower:flowers) {
			writer.print(convertToWML(flower));
		}
		writer.print("</flowers>");
		writer.close();
	}

	private String convertToWML(Flower flower){
		return "<flower>\n<name>"+ flower.getName() + "</name>\n<soil>" +
				flower.getSoil() + "</soil>\n<origin>" +
				flower.getOrigin() + "</origin>\n<visualParameters>\n<stemColour>" +
				flower.getStemColour() + "</stemColour>\n<leafColour>" +
				flower.getLeafColour() + "</leafColour>\n<aveLenFlower measure=\"cm\">" +
				flower.getCm() + "</aveLenFlower>\n</visualParameters>\n<growingTips>\n<tempreture measure=\"celcius\">" +
				flower.getCelcius() + "</tempreture>\n<lighting lightRequiring=" +
				(flower.isLightRequiring()?"\"no\"":"\"yes\"") + "/>\n<watering measure=\"mlPerWeek\">" +
				flower.getMlPerWeek() + "</watering>\n</growingTips>\n<multiplying>" +
				flower.getMultiplying() + "</multiplying>\n</flower>";
	}
}