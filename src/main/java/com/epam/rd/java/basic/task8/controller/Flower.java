package com.epam.rd.java.basic.task8.controller;

public class Flower {
    private String name, soil, origin, stemColour, leafColour, multiplying;
    private int cm, celcius, mlPerWeek;
    private boolean lightRequiring;

    Flower(String name, String soil, String origin, String stemColour,
           String leafColour, int cm, int celcius, boolean lightRequiring,
           int mlPerWeek, String multiplying){
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.cm = cm;
        this.celcius = celcius;
        this.lightRequiring = lightRequiring;
        this.mlPerWeek = mlPerWeek;
        this.multiplying = multiplying;
    }
    Flower(){}
    public String getName() {
        return name;
    }
    public String getSoil() {
        return soil;
    }
    public String getOrigin() {
        return origin;
    }
    public String getStemColour() {
        return stemColour;
    }
    public String getLeafColour() {
        return leafColour;
    }
    public int getCm() {
        return cm;
    }
    public int getCelcius() {
        return celcius;
    }
    public boolean isLightRequiring() {
        return lightRequiring;
    }
    public int getMlPerWeek() {
        return mlPerWeek;
    }
    public String getMultiplying() {
        return multiplying;
    }

    public void setCm(int cm) {
        this.cm = cm;
    }
    public void setCelcius(int celcius) {
        this.celcius = celcius;
    }
    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }
    public void setLightRequiring(boolean lightRequiring) {
        this.lightRequiring = lightRequiring;
    }
    public void setMlPerWeek(int mlPerWeek) {
        this.mlPerWeek = mlPerWeek;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
    public void setSoil(String soil) {
        this.soil = soil;
    }
    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }
    public void setName(String name) {
        this.name = name;
    }
}
